class Product < ApplicationRecord
    belongs_to :user
    validates :title, :description, presence: { message: "Khong duoc de trong" }
    validates :price, :numericality => {:greater_than_or_equal_to =>1.0, message: "Lon hon hoac bang 1"}
    validates :title, uniqueness: { message: "Khong duoc de trung" }
end
