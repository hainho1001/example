class User < ApplicationRecord
    has_many :products, dependent: :destroy, inverse_of: :user 
end
